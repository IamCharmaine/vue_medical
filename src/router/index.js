import Vue from 'vue'
import Router from 'vue-router'

// This will lazy load the component, it means the component will only be loaded if it is used
import Amputation from '@/components/medical/amputation/Amputation'
import Ankle from '@/components/medical/ankle/Ankle'
import Aid from '@/components/medical/aid/Aid'
import Artery from '@/components/medical/artery/Artery'
import Arthritis from '@/components/medical/arthritis/Arthritis'
import Bone from '@/components/medical/bone/Bone'
import Breast from '@/components/medical/breast/Breast'
import ChronicFatigue from '@/components/medical/chronicFatigue/ChronicFatigue'
import Cns from '@/components/medical/cns/Cns'
import ColdInjury from '@/components/medical/coldInjury/ColdInjury'
import CranialNerve from '@/components/medical/cranialNerve/CranialNerve'
import CSpine from '@/components/medical/cspine/CSpine'
import Dashboard from '@/components/shared/Dashboard'
import Diabetes from '@/components/medical/diabetes/Diabetes'
import DiabeticNeuropathy from '@/components/medical/diabeticNeuropathy/DiabeticNeuropathy'
import EarCondition from '@/components/medical/earCondition/EarCondition'
import Elbow from '@/components/medical/elbow/Elbow'
import Endocrine from '@/components/medical/endocrine/Endocrine'
import Esophageal from '@/components/medical/esophageal/Esophageal'
import Fibromyalgia from '@/components/medical/fibromyalgia/Fibromyalgia'
import Foot from '@/components/medical/foot/Foot'
import GallBladderPancreas from '@/components/medical/gallBladderPancreas/GallBladderPancreas'
import GenMedCompensation from '@/components/medical/genMedCompensation/GenMedCompensation'
import Gyno from '@/components/medical/gyno/Gyno'
import GenMedGulfWar from '@/components/medical/genMedGulfWar/GenMedGulfWar'
import Hand from '@/components/medical/hand/Hand'
import Headache from '@/components/medical/headache/Headache'
import Heart from '@/components/medical/heart/Heart'
import HemaLymphatic from '@/components/medical/hemaLymphatic/HemaLymphatic'
import Hernia from '@/components/medical/hernia/Hernia'
import HipThigh from '@/components/medical/hipThigh/HipThigh'
import HipThighOld from '@/components/medical/hipThighOld/HipThighOld'
import Hypertension from '@/components/medical/hypertension/Hypertension'
import Infectious from '@/components/medical/infectious/Infectious'
import IntestinalSx from '@/components/medical/intestinalSx/IntestinalSx'
import Intestines from '@/components/medical/intestines/Intestines'
import IntestinesInfectious from '@/components/medical/intestinesInfectious/IntestinesInfectious'
import Kidney from '@/components/medical/kidney/Kidney'
import Knee from '@/components/medical/knee/Knee'
import MedicalTemplate from '@/components/medical/MedicalTemplate'
import MuscleInjury from '@/components/medical/muscle/MuscleInjury'
import MedicalPage from '@/pages/MedicalPage'
import Profile from '@/components/medical/Profile'
import Shoulder from '@/components/medical/shoulder/Shoulder'
import Tmj from '@/components/medical/tmj/Tmj'
import TSpine from '@/components/medical/tspine/TSpine'
Vue.use(Router)

let router = new Router({
  mode: 'history', // This will remove the `#/` in the url
  routes: [
    {
      path: '/',
      component: MedicalPage,
      children: [
        {
          path: '/',
          name: 'dashboard',
          component: Dashboard
        },
        {
          path: 'medical-template',
          name: 'medical-template',
          component: MedicalTemplate,
          children: [
            {
              path: 'profile',
              name: 'profile',
              component: Profile
            },
            {
              path: 'amputation',
              name: 'amputation',
              component: Amputation
            },
            {
              path: 'ankle',
              name: 'ankle',
              component: Ankle
            },
            {
              path: 'aid',
              name: 'aid',
              component: Aid
            },
            {
              path: 'artery',
              name: 'artery',
              component: Artery
            },
            {
              path: 'non-degen-arthritis',
              name: 'non-degen-arthritis',
              component: Arthritis
            },
            {
              path: 'bone',
              name: 'bone',
              component: Bone
            },
            {
              path: 'breast',
              name: 'breast',
              component: Breast
            },
            {
              path: 'chronic-fatigue',
              name: 'chronic-fatigue',
              component: ChronicFatigue
            },
            {
              path: 'cns',
              name: 'cns',
              component: Cns
            },
            {
              path: 'cold-injury',
              name: 'cold-injury',
              component: ColdInjury
            },
            {
              path: 'cranial-nerve',
              name: 'cranial-nerve',
              component: CranialNerve
            },
            {
              path: 'c-spine',
              name: 'c-spine',
              component: CSpine
            },
            {
              path: 'diabetes',
              name: 'diabetes',
              component: Diabetes
            },
            {
              path: 'diabetic-neuropathy',
              name: 'diabetic-neuropathy',
              component: DiabeticNeuropathy
            },
            {
              path: 'ear-condition',
              name: 'ear-condition',
              component: EarCondition
            },
            {
              path: 'elbow',
              name: 'elbow',
              component: Elbow
            },
            {
              path: 'endocrine',
              name: 'endocrine',
              component: Endocrine
            },
            {
              path: 'esophageal',
              name: 'esophageal',
              component: Esophageal
            },
            {
              path: 'fibromyalgia',
              name: 'fibromyalgia',
              component: Fibromyalgia
            },
            {
              path: 'foot',
              name: 'foot',
              component: Foot
            },
            {
              path: 'gall-bladder-pancreas',
              name: 'gall-bladder-pancreas',
              component: GallBladderPancreas
            },
            {
              path: 'gen-med-compensation',
              name: 'gen-med-compensation',
              component: GenMedCompensation
            },
            {
              path: 'gen-med-gulf-war',
              name: 'gen-med-gulf-war',
              component: GenMedGulfWar
            },
            {
              path: 'gyno',
              name: 'gyno',
              component: Gyno
            },
            {
              path: 'hand-and-finger',
              name: 'hand',
              component: Hand
            },
            {
              path: 'headache',
              name: 'headache',
              component: Headache
            },
            {
              path: 'heart',
              name: 'heart',
              component: Heart
            },
            {
              path: 'hema-lymphatic',
              name: 'hema-lymphatic',
              component: HemaLymphatic
            },
            {
              path: 'hernia',
              name: 'hernia',
              component: Hernia
            },
            {
              path: 'hip-thigh',
              name: 'hip-thigh',
              component: HipThigh
            },
            {
              path: 'hip-thigh-old',
              name: 'hip-thigh-old',
              component: HipThighOld
            },
            {
              path: 'hypertension',
              name: 'hypertension',
              component: Hypertension
            },
            {
              path: 'infectious',
              name: 'infectious',
              component: Infectious
            },
            {
              path: 'intestinal-sx',
              name: 'intestinal-sx',
              component: IntestinalSx
            },
            {
              path: 'intestines',
              name: 'intestines',
              component: Intestines
            },
            {
              path: 'intestines-infectious',
              name: 'intestines-infectious',
              component: IntestinesInfectious
            },
            {
              path: 'kidney',
              name: 'kidney',
              component: Kidney
            },
            {
              path: 'knee',
              name: 'knee',
              component: Knee
            },
            {
              path: 'muscle-injury',
              name: 'muscle-injury',
              component: MuscleInjury
            },
            {
              path: 'shoulder',
              name: 'shoulder',
              component: Shoulder
            },
            {
              path: 'tmj',
              name: 'tmj',
              component: Tmj
            },
            {
              path: 'tspine',
              name: 'tspine',
              component: TSpine
            }
          ]
        }
      ]
    }
  ]
})

export default router
