// Contains all of the events used in our global event bus
export const EVENT_NAVIGATION_DRAWER = 'event_navigation_drawer'
export const EVENT_MINIVARIANT = 'event_minivariant'
export const EVENT_CLIPPED = 'event_clipped'
