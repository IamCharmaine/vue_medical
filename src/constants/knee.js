export default {
  data: () => ({
    part1: {
      finalDiagnosis: {
        title: 'Final Diagnosis by Dr. Broomes:',
        model: null,
        field: 'textfield',
        type: 'text'
      },
      diagnosis: {
        title: 'Diagnosis',
        model: null,
        field: 'textfield',
        type: 'text'
      }
    },
    part2: {
      rightDateOfOnset: {
        title: 'Date of onset',
        model: null,
        field: 'dateField'
      },
      leftDateOfOnset: {
        title: 'Date of onset',
        model: null,
        field: 'dateField'
      }
    },
    part3: {
      rightDetailsOfOnset: {
        title: 'Details of Onset',
        model: null,
        field: 'textarea'
      },
      leftDetailsOfOnset: {
        title: 'Details of Onset',
        model: null,
        field: 'textarea'
      },
      rightCurrentSymptoms: {
        title: 'Have the symtoms improved, progressed or stayed the same ?',
        model: null,
        field: 'textarea'
      },
      leftCurrentSymptoms: {
        title: 'Have the symtoms improved, progressed or stayed the same ?',
        model: null,
        field: 'textarea'
      }
    },
    part4: {
      medications: {
        title: 'Medications',
        model: null,
        field: 'textarea'
      },
      priorTreatments: {
        title: 'Prior Treatments',
        model: null,
        field: 'textarea'
      }
    },
    part5: {
      veteranReport1: {
        title: 'Does the Veteran report that flare-ups impact the function of the hip/thigh? (Include duration, frequency and severity)',
        text: 'NOTE: "Flare up" is defined as an acute deviation from the baseline.',
        model: null,
        field: 'textarea'
      },
      veteranReport2: {
        title: 'Does the Veteran report having any functional loss or functional impairment not limited to repeated use over time?',
        model: null,
        field: 'textarea'
      },
      limitation1: {
        title: 'Further limitations during a flare-up',
        model: null,
        field: 'textarea'
      },
      limitation2: {
        title: 'Further limitations after repetitive use',
        model: null,
        field: 'textarea'
      }
    },
    part6: {
      evidence: {
        title: 'Evidence',
        model: null,
        field: 'textarea'
      }
    },
    part7: {
      subluxationSelect: {
        title: 'Recurrent subluxation(partial dislocation)',
        model: null,
        field: 'select'
      },
      subluxationArea: {
        model: null,
        field: 'textarea'
      }
    },
    part8: {
      instabilitySelect: {
        title: 'Lateral instability',
        model: null,
        field: 'select'
      },
      instabilityArea: {
        title: 'Lateral instability',
        model: null,
        field: 'textarea'
      }
    },
    part9: {
      effusionSelect: {
        title: 'Recurrent effusion',
        model: null,
        field: 'select'
      },
      effusionArea: {
        model: null,
        field: 'textarea'
      }
    },
    part10: {
      dislocationSelect: {
        title: 'Recurrent patellar dislocation',
        model: null,
        field: 'select'
      },
      dislocationArea: {
        model: null,
        field: 'textarea'
      }
    },
    part11: {
      syndromeSelect: {
        title: '“Shin splints” (medial tibial stress syndrome)',
        model: null,
        field: 'select'
      },
      syndromeArea: {
        model: null,
        field: 'textarea'
      }
    },
    part12: {
      ankleBtn: {
        title: 'If the answer to the previous question is yes, do the shin aplints affect the ankle ROM?',
        model: null,
        field: 'button'
      },
      ankleArea: {
        model: null,
        field: 'textarea'
      }
    },
    part13: {
      fractureSelect: {
        title: 'Stress fracture of the lower leg',
        model: null,
        field: 'select'
      },
      fractureArea: {
        model: null,
        field: 'textarea'
      }
    },
    part14: {
      compartmentSyndromeSelect: {
        title: 'Chronic exertional compartment syndrome',
        model: null,
        field: 'select'
      },
      compartmentSyndromeArea: {
        model: null,
        field: 'textarea'
      }
    },
    part15: {
      veteranConditionSelect: {
        title: 'Acquired and/or traumatic genu recurvatum with objectively demonstrated weakness and insecurity in weight-bearing.',
        model: null,
        field: 'select'
      },
      veteranConditionArea: {
        model: null,
        field: 'textarea'
      }
    },
    part16: {
      MeniscalDislocationSelect: {
        title: 'Meniscal dislocation',
        model: null,
        field: 'select'
      },
      MeniscalDislocationArea: {
        model: null,
        field: 'textarea'
      }
    },
    part17: {
      MeniscalTearSelect: {
        title: 'Meniscal tear',
        model: null,
        field: 'select'
      },
      MeniscalTearArea: {
        model: null,
        field: 'textarea'
      }
    },
    part18: {
      lockingSelect: {
        title: 'Frequent episodes of joint “locking”',
        model: null,
        field: 'select'
      },
      lockingArea: {
        model: null,
        field: 'textarea'
      }
    },
    part19: {
      painSelect: {
        title: 'Frequent episodes of joint pain',
        model: null,
        field: 'select'
      },
      painArea: {
        model: null,
        field: 'textarea'
      }
    },
    part20: {
      xRaysDate: {
        title: 'X-RAYS',
        model: null,
        responsive: 'xs12 md3',
        field: 'dateField'
      },
      xRaysDateSelect: {
        model: null,
        responsive: 'xs12 md3',
        field: 'select'
      },
      xRaysArea: {
        model: null,
        responsive: 'xs12 md3',
        field: 'textarea'
      },
      xRaysText: {
        model: null,
        responsive: 'xs12 md1',
        field: 'textfield',
        type: 'number'
      }
    },
    part21: {
      mriDate: {
        title: 'MRI',
        model: null,
        responsive: 'xs12 md3',
        field: 'dateField'
      },
      mriSelect: {
        model: null,
        responsive: 'xs12 md3',
        field: 'select'
      },
      mriArea: {
        model: null,
        responsive: 'xs12 md3',
        field: 'textarea'
      },
      mriText: {
        model: null,
        responsive: 'xs12 md1',
        field: 'textfield',
        type: 'number'
      }
    },
    part22: {
      ctScanDate: {
        title: 'CT SCAN',
        model: null,
        responsive: 'xs12 md3',
        field: 'dateField'
      },
      ctScanSelect: {
        model: null,
        responsive: 'xs12 md3',
        field: 'select'
      },
      ctScanArea: {
        model: null,
        responsive: 'xs12 md3',
        field: 'textarea'
      },
      ctScanText: {
        model: null,
        responsive: 'xs12 md1',
        field: 'textfield',
        type: 'number'
      }
    },
    part23: {
      othersTypeText: {
        title: 'OTHERS',
        model: null,
        responsive: 'xs12 md2',
        field: 'textfield',
        type: 'text'
      },
      othersDate: {
        model: null,
        responsive: 'xs12 md3',
        field: 'dateField'
      },
      othersSelect: {
        model: null,
        responsive: 'xs12 md3',
        field: 'select'
      },
      othersArea: {
        model: null,
        responsive: 'xs12 md3',
        field: 'textarea'
      },
      othersTypeNumber: {
        model: null,
        responsive: 'xs12 md1',
        field: 'textfield',
        type: 'number'
      }
    },
    part24: {
      procedure1Date: {
        model: null,
        responsive: 'xs12 md3',
        field: 'dateField'
      },
      procedure1TypeText: {
        model: null,
        responsive: 'xs12 md4',
        field: 'textfield',
        type: 'text'
      },
      procedure1Select: {
        model: null,
        responsive: 'xs12 md3',
        field: 'select'
      },
      procedure1TypeNumber: {
        model: null,
        responsive: 'xs12 md2',
        field: 'textfield',
        type: 'number'
      }
    },
    part25: {
      procedure2Date: {
        model: null,
        responsive: 'xs12 md3',
        field: 'dateField'
      },
      procedure2TypeText: {
        model: null,
        responsive: 'xs12 md4',
        field: 'textfield',
        type: 'text'
      },
      procedure2Select: {
        model: null,
        responsive: 'xs12 md3',
        field: 'select'
      },
      procedure2TypeNumber: {
        model: null,
        responsive: 'xs12 md2',
        field: 'textfield',
        type: 'number'
      }
    },
    part26: {
      wheelchairSelect: {
        title: 'Wheelchair',
        model: null,
        responsive: 'xs12 md3',
        field: 'select'
      },
      wheelchairSelect1: {
        model: null,
        responsive: 'xs12 md3',
        field: 'select'
      },
      wheelchairArea: {
        model: null,
        responsive: 'xs12 md4',
        field: 'textarea'
      }
    },
    part27: {
      braceSelect: {
        title: 'Brace',
        model: null,
        responsive: 'xs12 md3',
        field: 'select'
      },
      braceSelect1: {
        model: null,
        responsive: 'xs12 md3',
        field: 'select'
      },
      braceArea: {
        model: null,
        responsive: 'xs12 md4',
        field: 'textarea'
      }
    },
    part28: {
      crutchesSelect: {
        title: 'Crutches',
        model: null,
        responsive: 'xs12 md3',
        field: 'select'
      },
      crutchesSelect1: {
        model: null,
        responsive: 'xs12 md3',
        field: 'select'
      },
      crutchesArea: {
        model: null,
        responsive: 'xs12 md4',
        field: 'textarea'
      }
    },
    part29: {
      caneSelect: {
        title: 'Cane',
        model: null,
        responsive: 'xs12 md3',
        field: 'select'
      },
      caneSelect1: {
        model: null,
        responsive: 'xs12 md3',
        field: 'select'
      },
      caneArea: {
        model: null,
        responsive: 'xs12 md4',
        field: 'textarea'
      }
    },
    part30: {
      walkerSelect: {
        title: 'Walker',
        model: null,
        responsive: 'xs12 md3',
        field: 'select'
      },
      walkerSelect1: {
        model: null,
        responsive: 'xs12 md3',
        field: 'select'
      },
      walkereArea: {
        model: null,
        responsive: 'xs12 md4',
        field: 'textarea'
      }
    },
    part31: {
      othersTypeText: {
        model: null,
        responsive: 'xs12 md2',
        field: 'textfield',
        type: 'text'
      },
      othersSelect: {
        model: null,
        responsive: 'xs12 md3',
        field: 'select'
      },
      othersSelect1: {
        model: null,
        responsive: 'xs12 md3',
        field: 'select'
      },
      othersArea: {
        model: null,
        responsive: 'xs12 md4',
        field: 'textarea'
      }
    },
    part32: {
      rightFlexionActiveTypeText: {
        title: 'Active',
        model: null,
        responsive: 'xs12 md3',
        field: 'textfield',
        type: 'text'
      },
      rightFlexionActiveTypeNumber: {
        model: null,
        responsive: 'xs12 md3',
        field: 'textfield',
        type: 'number'
      },
      rightFlexionactiveArea: {
        model: null,
        responsive: 'xs12 md3',
        field: 'textarea'
      }
    },
    part33: {
      rightFlexionPassiveTypeText: {
        title: 'Passive',
        model: null,
        responsive: 'xs12 md3',
        field: 'textfield',
        type: 'text'
      },
      rightFlexionPassiveTypeNumber: {
        model: null,
        responsive: 'xs12 md3',
        field: 'textfield',
        type: 'number'
      },
      rightFlexionPassiveArea: {
        model: null,
        responsive: 'xs12 md3',
        field: 'textarea'
      }
    },
    part34: {
      rightExtensionActiveTypeText: {
        title: 'Active',
        model: null,
        responsive: 'xs12 md3',
        field: 'textfield',
        type: 'text'
      },
      rightExtensionActiveTypeNumber: {
        model: null,
        responsive: 'xs12 md3',
        field: 'textfield',
        type: 'number'
      },
      rightExtensionActiveArea: {
        model: null,
        responsive: 'xs12 md3',
        field: 'textarea'
      }
    },
    part35: {
      rightExtensionPassiveTypeText: {
        title: 'Passive',
        model: null,
        responsive: 'xs12 md3',
        field: 'textfield',
        type: 'text'
      },
      lrightExtensionPassiveTypeNumber: {
        model: null,
        responsive: 'xs12 md3',
        field: 'textfield',
        type: 'number'
      },
      rightExtensionPassiveArea: {
        model: null,
        responsive: 'xs12 md3',
        field: 'textarea'
      }
    },
    part36: {
      leftFlexionActiveTypeText: {
        title: 'Active',
        model: null,
        responsive: 'xs12 md3',
        field: 'textfield',
        type: 'text'
      },
      leftFlexionActiveTypeNumber: {
        model: null,
        responsive: 'xs12 md3',
        field: 'textfield',
        type: 'number'
      },
      leftFlexionActiveArea: {
        model: null,
        responsive: 'xs12 md3',
        field: 'textarea'
      }
    },
    part37: {
      leftFlexionPassiveTypeText: {
        title: 'Passive',
        model: null,
        responsive: 'xs12 md3',
        field: 'textfield',
        type: 'text'
      },
      leftFlexionPassiveTypeNumber: {
        model: null,
        responsive: 'xs12 md3',
        field: 'textfield',
        type: 'number'
      },
      leftFlexionPassiveArea: {
        model: null,
        responsive: 'xs12 md3',
        field: 'textarea'
      }
    },
    part38: {
      leftExtensionActiveTypeText: {
        title: 'Active',
        model: null,
        responsive: 'xs12 md3',
        field: 'textfield',
        type: 'text'
      },
      leftExtensionActiveTypeNumber: {
        model: null,
        responsive: 'xs12 md3',
        field: 'textfield',
        type: 'number'
      },
      leftExtensionActiveArea: {
        model: null,
        responsive: 'xs12 md3',
        field: 'textarea'
      }
    },
    part39: {
      leftExtensionPassiveTypeText: {
        title: 'Passive',
        model: null,
        responsive: 'xs12 md3',
        field: 'textfield',
        type: 'text'
      },
      leftExtensionPassiveTypeNumber: {
        model: null,
        responsive: 'xs12 md3',
        field: 'textfield',
        type: 'number'
      },
      leftExtensionPassiveArea: {
        model: null,
        responsive: 'xs12 md3',
        field: 'textarea'
      }
    },
    part40: {
      lachmanTest: {
        title: 'LACHMAN TEST (ANTERIOR INSTABILITY)',
        text: 'click link to watch video demo',
        lachmanTestSelect: {
          model: null,
          responsive: 'xs12 md4',
          field: 'select'
        },
        lachmanTestSelect1: {
          model: null,
          responsive: 'xs12 md4',
          field: 'select'
        },
        lachmanTestArea: {
          model: null,
          responsive: 'xs12 md4',
          field: 'textarea'
        }
      }
    },
    part41: {
      posteriorTest: {
        title: 'POSTERIOR INSTABILITY (POSTERIOR DRAWER TEST)',
        text: 'click link to watch video demo',
        posteriorTestSelect: {
          model: null,
          responsive: 'xs12 md4',
          field: 'select'
        },
        posteriorTestSelect1: {
          model: null,
          responsive: 'xs12 md4',
          field: 'select'
        },
        posteriorTestArea: {
          model: null,
          responsive: 'xs12 md4',
          field: 'textarea'
        }
      }
    },
    part42: {
      medialTest: {
        title: 'MEDIAL INSTABILITY (VALGUS STRESS TEST FOR MCL INJURY)',
        text: 'click link to watch video demo',
        medialTestSelect: {
          model: null,
          responsive: 'xs12 md4',
          field: 'select'
        },
        medialTestSelect1: {
          model: null,
          responsive: 'xs12 md4',
          field: 'select'
        },
        medialTestArea: {
          model: null,
          responsive: 'xs12 md4',
          field: 'textarea'
        }
      }
    },
    part43: {
      lateralTest: {
        title: 'LATERAL INSTABILITY (VARUS STRESS TEST FOR LCL INJURY)',
        text: 'click link to watch video demo',
        lateralTestSelect: {
          model: null,
          responsive: 'xs12 md4',
          field: 'select'
        },
        lateralTestSelect1: {
          model: null,
          responsive: 'xs12 md4',
          field: 'select'
        },
        lateralTestArea: {
          model: null,
          responsive: 'xs12 md4',
          field: 'textarea'
        }
      }
    },
    part44: {
      crepitus: {
        title: 'Crepitus',
        crepitusButton: {
          model: null,
          field: 'button'
        },
        crepitusButton1: {
          model: null,
          field: 'button'
        },
        crepitusArea: {
          model: null,
          field: 'textarea'
        }
      }
    },
    part45: {
      tenderness: {
        title: 'Tenderness',
        tendernessButton: {
          model: null,
          field: 'button',
          rightTendernessLocation: {
            title: 'Location',
            model: null,
            field: 'textfield',
            type: 'text'
          },
          rightTendernessSeverity: {
            title: 'Severity',
            model: null,
            field: 'textfield',
            type: 'text'
          }
        },
        tendernessButton1: {
          model: null,
          field: 'button',
          leftTendernessLocation: {
            title: 'Location',
            model: null,
            field: 'textfield',
            type: 'text'
          },
          leftTendernessSeverity: {
            title: 'Severity',
            model: null,
            field: 'textfield',
            type: 'text'
          }
        },
        tendernessArea: {
          model: null,
          field: 'textarea'
        }
      }
    },
    part46: {
      conditionButton: {
        title: 'Which condition Is the primary cause of tenderness?',
        model: null,
        field: 'button'
      },
      conditionButton1: {
        title: 'Which condition Is the primary cause of tenderness?',
        model: null,
        field: 'button'
      },
      conditionArea: {
        model: null,
        field: 'textarea'
      }
    },
    part47: {
      evidenceOneButton: {
        title: 'Objective evidence of pain on weight-bearing',
        model: null,
        field: 'button'
      },
      evidenceOneButton1: {
        title: 'Objective evidence of pain on weight-bearing',
        model: null,
        field: 'button'
      },
      evidenceOneArea: {
        title: 'Objective evidence of pain on weight-bearing',
        model: null,
        field: 'textarea'
      }
    },
    part48: {
      evidenceTwoButton: {
        title: 'Objective evidence of pain on non-weight-bearing',
        model: null,
        field: 'button'
      },
      evidenceTwoButton1: {
        title: 'Objective evidence of pain on non-weight-bearing',
        model: null,
        field: 'button'
      },
      evidenceTwoArea: {
        title: 'Objective evidence of pain on non-weight-bearing',
        model: null,
        field: 'textarea'
      }
    },
    part49: {
      swellingButton: {
        title: 'Swelling',
        model: null,
        field: 'button'
      },
      swellingButton1: {
        title: 'Swelling',
        model: null,
        field: 'button'
      },
      swellingArea: {
        title: 'Swelling',
        model: null,
        field: 'textarea'
      }
    },
    part50: {
      atrophy: {
        title: 'Atrophy',
        atrophyButton: {
          model: null,
          field: 'button',
          rightAtrophyLocation: {
            title: 'Location',
            model: null,
            field: 'textfield',
            type: 'text'
          },
          rightAtrophyMeasurement: {
            title: 'Measurement',
            model: null,
            field: 'textfield',
            type: 'text'
          }
        },
        atrophyButton1: {
          model: null,
          field: 'button',
          leftAtrophyLocation: {
            title: 'Location',
            model: null,
            field: 'textfield',
            type: 'text'
          },
          leftAtrophyMeasurement: {
            title: 'Measurement',
            model: null,
            field: 'textfield',
            type: 'text'
          }
        },
        atrophyArea: {
          model: null,
          field: 'textarea'
        }
      }
    },
    part51: {
      deformityButton: {
        title: 'Deformity',
        model: null,
        field: 'button'
      },
      deformityButton1: {
        title: 'Deformity',
        model: null,
        field: 'button'
      },
      deformityArea: {
        title: 'Deformity',
        model: null,
        field: 'textarea'
      }
    },
    part52: {
      gaitButton: {
        title: 'Gait',
        model: null,
        field: 'button',
        rightGaitSeverity: {
          title: 'Severity',
          model: null,
          field: 'textfield',
          type: 'text'
        }
      },
      gaitButton1: {
        title: 'Gait',
        model: null,
        field: 'button',
        leftGaitSeverity: {
          title: 'Severity',
          model: null,
          field: 'textfield',
          type: 'text'
        }
      },
      gaitArea: {
        title: 'Gait',
        model: null,
        field: 'textarea'
      }
    },
    part53: {
      legButton: {
        title: 'Leg Length Discrepancy',
        model: null,
        field: 'button',
        rightLegMeasurement: {
          title: 'Measurement',
          model: null,
          field: 'textfield',
          type: 'text'
        }
      },
      legButton1: {
        title: 'Leg Length Discrepancy',
        model: null,
        field: 'button',
        leftLegMeasurement: {
          title: 'Measurement',
          model: null,
          field: 'textfield',
          type: 'text'
        }
      },
      legArea: {
        title: 'Leg Length Discrepancy',
        model: null,
        field: 'textarea'
      }
    },
    part54: {
      scar: {
        title: 'Scar',
        scarButton: {
          model: null,
          field: 'button',
          rightScarLocation: {
            title: 'Location',
            model: null,
            field: 'textfield',
            type: 'text'
          },
          rightScarMeasurement: {
            title: 'Measurement',
            model: null,
            field: 'textfield',
            type: 'text'
          }
        },
        scarButton1: {
          model: null,
          field: 'button',
          leftScarLocation: {
            title: 'Location',
            model: null,
            field: 'textfield',
            type: 'text'
          },
          leftScarMeasurement: {
            title: 'Measurement',
            model: null,
            field: 'textfield',
            type: 'text'
          }
        },
        scarArea: {
          model: null,
          field: 'textarea'
        }
      }
    },
    part55: {
      scarIssueButton: {
        title: 'Is the scar painful or unstable; have a total area equal to or greater than 39 square cm (6 square inches); or are located on the head, face or neck?',
        model: null,
        field: 'button'
      },
      scarIssueButton1: {
        title: 'Is the scar painful or unstable; have a total area equal to or greater than 39 square cm (6 square inches); or are located on the head, face or neck?',
        model: null,
        field: 'button'
      },
      scarIssueArea: {
        model: null,
        field: 'textarea'
      }
    },
    part56: {
      angleArea: {
        title: 'Favorable angle in full extension or in slight flexion between 0 and 10 degrees',
        model: null,
        responsive: 'xs12 md3',
        field: 'textarea'
      },
      angleArea1: {
        title: 'Favorable angle in full extension or in slight flexion between 0 and 10 degrees',
        model: null,
        responsive: 'xs12 md3',
        field: 'textarea'
      },
      angleArea2: {
        title: 'Favorable angle in full extension or in slight flexion between 0 and 10 degrees',
        model: null,
        responsive: 'xs12 md6',
        field: 'textarea'
      }
    },
    part57: {
      flexionOneArea: {
        title: 'In flexion between 10 and 20 degrees',
        model: null,
        responsive: 'xs12 md3',
        field: 'textarea'
      },
      flexionOneArea1: {
        title: 'In flexion between 10 and 20 degrees',
        model: null,
        responsive: 'xs12 md3',
        field: 'textarea'
      },
      flexionOneArea2: {
        title: 'In flexion between 10 and 20 degrees',
        model: null,
        responsive: 'xs12 md6',
        field: 'textarea'
      }
    },
    part58: {
      flexionTwoArea: {
        title: 'In flexion between 20 and 45 degrees',
        model: null,
        responsive: 'xs12 md3',
        field: 'textarea'
      },
      flexionTwoArea1: {
        title: 'In flexion between 20 and 45 degrees',
        model: null,
        responsive: 'xs12 md3',
        field: 'textarea'
      },
      flexionTwoArea2: {
        title: 'In flexion between 20 and 45 degrees',
        model: null,
        responsive: 'xs12 md6',
        field: 'textarea'
      }
    },
    part59: {
      flexionThreeArea: {
        title: 'Extremely unfavorable, in flexion at an angle of 45 degrees or more',
        model: null,
        responsive: 'xs12 md3',
        field: 'textarea'
      },
      flexionThreeArea1: {
        title: 'Extremely unfavorable, in flexion at an angle of 45 degrees or more',
        model: null,
        responsive: 'xs12 md3',
        field: 'textarea'
      },
      flexionThreeArea2: {
        title: 'Extremely unfavorable, in flexion at an angle of 45 degrees or more',
        model: null,
        responsive: 'xs12 md6',
        field: 'textarea'
      }
    },
    part60: {
      kneeQuestionOneButton: {
        title: 'Is the Veteran being examined immediately after repetitive use over time?',
        model: null,
        field: 'button'
      },
      kneeQuestionOneButton1: {
        title: 'Is the Veteran being examined immediately after repetitive use over time?',
        model: null,
        field: 'button'
      }
    },
    part61: {
      kneeQuestionTwoButton: {
        title: 'If the examination is medically inconsistent with the Veterans statements of functional loss, please explain:',
        model: null,
        field: 'textarea'
      },
      kneeQuestionTwoButton1: {
        title: 'If the examination is medically inconsistent with the Veterans statements of functional loss, please explain:',
        model: null,
        field: 'textarea'
      }
    },
    part62: {
      kneeQuestionThreeButton: {
        title: 'If the examination is medically inconsistent with the Veterans statements of functional loss, please explain:',
        model: null,
        field: 'button'
      },
      kneeQuestionThreeButton1: {
        title: 'If the examination is medically inconsistent with the Veterans statements of functional loss, please explain:',
        model: null,
        field: 'button'
      }
    },
    part63: {
      kneeQuestionFourArea: {
        title: 'If unable to say without mere speculation, please explain:',
        model: null,
        field: 'textarea'
      },
      kneeQuestionFourArea1: {
        title: 'If unable to say without mere speculation, please explain:',
        model: null,
        field: 'textarea'
      }
    },
    part64: {
      kneeQuestionFiveCheckBox: {
        title: 'Select all factors that cause this functional loss:',
        model: null,
        field: 'checkbox'
      },
      kneeQuestionFiveCheckBox1: {
        title: 'Select all factors that cause this functional loss:',
        model: null,
        field: 'checkbox'
      },
      kneeQuestionFiveText: {
        title: 'Select all factors that cause this functional loss:',
        model: null,
        field: 'textfield',
        label: 'Other/s',
        type: 'text'
      },
      kneeQuestionFiveText1: {
        title: 'Select all factors that cause this functional loss:',
        model: null,
        field: 'textfield',
        label: 'Other/s',
        type: 'text'
      }
    },
    part65: {
      kneeQuestionSixButton: {
        title: 'If unable to say without mere speculation, please explain:',
        model: null,
        field: 'button'
      },
      kneeQuestionSixButton1: {
        title: 'If unable to say without mere speculation, please explain:',
        model: null,
        field: 'button'
      }
    },
    part66: {
      flareUpsQuestionButton: {
        title: 'Is the examination being conducted during a flare up?',
        model: null,
        field: 'button'
      },
      flareUpsQuestionButton1: {
        title: 'Is the examination being conducted during a flare up?',
        model: null,
        field: 'button'
      }
    },
    part67: {
      flareUpsQuestion1Area: {
        title: 'If the examination is medically inconsistent with the Veterans statements of functional loss, please explain:',
        model: null,
        field: 'textarea'
      },
      flareUpsQuestion1Area1: {
        title: 'If the examination is medically inconsistent with the Veterans statements of functional loss, please explain:',
        model: null,
        field: 'textarea'
      }
    },
    part68: {
      flareUpsQuestion2Button: {
        title: '*Does pain, weakness, fatigability or incoordination significantly limit functional ability with flare-ups?',
        model: null,
        field: 'button'
      },
      flareUpsQuestion2Button1: {
        title: '*Does pain, weakness, fatigability or incoordination significantly limit functional ability with flare-ups?',
        model: null,
        field: 'button'
      }
    },
    part69: {
      flareUpsQuestion3Area: {
        title: 'If unable to say without mere speculation, please explain:',
        model: null,
        field: 'textarea'
      },
      flareUpsQuestion3Area1: {
        title: 'If unable to say without mere speculation, please explain:',
        model: null,
        field: 'textarea'
      }
    },
    part70: {
      flareUpsQuestion4CheckBox: {
        title: 'Select all factors that cause this functional loss:',
        model: null,
        field: 'checkbox'
      },
      flareUpsQuestion4CheckBox1: {
        title: 'Select all factors that cause this functional loss:',
        model: null,
        field: 'checkbox'
      },
      flareUpsQuestion4Text: {
        title: 'Select all factors that cause this functional loss:',
        model: null,
        responsive: 'xs12',
        field: 'textfield',
        label: 'Other/s',
        type: 'text'
      },
      flareUpsQuestion4Text1: {
        title: 'Select all factors that cause this functional loss:',
        model: null,
        responsive: 'xs12',
        field: 'textfield',
        label: 'Other/s',
        type: 'text'
      }
    },
    part71: {
      flareUpsQuestion5Button: {
        title: 'Are you able to describe of Range Motion?',
        model: null,
        field: 'button',
        btns: ['Yes', 'No']
      },
      flareUpsQuestion5Button1: {
        title: 'Are you able to describe of Range Motion?',
        model: null,
        field: 'button',
        btns: ['Yes', 'No']
      }
    },
    part72: {
      noneCheckBox: {
        title: 'None',
        model: null,
        field: 'checkbox',
        items: [{
          label: '',
          model: null
        }]
      },
      noneCheckBox1: {
        title: 'None',
        model: null,
        field: 'checkbox',
        items: [{
          label: '',
          model: null
        }]
      },
      noneArea: {
        title: 'None',
        model: null,
        field: 'textarea'
      }
    },
    part73: {
      movementCheckBox: {
        title: 'Less movement than normal (due to ankylosis, limitation or blocking, adhesions, tendon-tie-ups, contracted scars, etc.)',
        model: null,
        field: 'checkbox',
        items: [{
          label: '',
          model: null
        }]
      },
      movementCheckBox1: {
        model: null,
        field: 'checkbox',
        items: [{
          label: '',
          model: null
        }]
      },
      movementArea: {
        model: null,
        field: 'textarea'
      }
    },
    part74: {
      movement1CheckBox: {
        title: 'More movement than normal (from flail joints, resections, nonunion of fractures, relaxation of ligaments, etc..)',
        model: null,
        field: 'checkbox',
        items: [{
          label: '',
          model: null
        }]
      },
      movement1CheckBox1: {
        model: null,
        field: 'checkbox',
        items: [{
          label: '',
          model: null
        }]
      },
      movement1Area: {
        model: null,
        field: 'textarea'
      }
    },
    part75: {
      movement2CheckBox: {
        title: 'Weakened movement (due to muscle injury, disease or injury of peripheral nerves, divided or lengthened tendons, etc.)',
        model: null,
        field: 'checkbox',
        items: [{
          label: '',
          model: null
        }]
      },
      movement2CheckBox1: {
        model: null,
        field: 'checkbox',
        items: [{
          label: '',
          model: null
        }]
      },
      movement2Area: {
        model: null,
        field: 'textarea'
      }
    },
    part76: {
      swellingCheckBox: {
        title: 'Swelling',
        model: null,
        field: 'checkbox',
        items: [{
          label: '',
          model: null
        }]
      },
      swellingCheckBox1: {
        title: 'Swelling',
        model: null,
        field: 'checkbox',
        items: [{
          label: '',
          model: null
        }]
      },
      swellingArea: {
        title: 'Swelling',
        model: null,
        field: 'textarea'
      }
    },
    part77: {
      deformityCheckBox: {
        title: 'Deformity',
        model: null,
        field: 'checkbox',
        items: [{
          label: '',
          model: null
        }]
      },
      deformityCheckBox1: {
        title: 'Deformity',
        model: null,
        field: 'checkbox',
        items: [{
          label: '',
          model: null
        }]
      },
      deformityArea: {
        title: 'Deformity',
        model: null,
        field: 'textarea'
      }
    },
    part78: {
      interferenceCheckBox: {
        title: 'Interference with sitting',
        model: null,
        field: 'checkbox',
        items: [{
          label: '',
          model: null
        }]
      },
      interferenceCheckBox1: {
        title: 'Interference with sitting',
        model: null,
        field: 'checkbox',
        items: [{
          label: '',
          model: null
        }]
      },
      interferenceArea: {
        title: 'Interference with sitting',
        model: null,
        field: 'textarea'
      }
    },
    part79: {
      interference1CheckBox: {
        title: 'Interference with standing',
        model: null,
        field: 'checkbox',
        items: [{
          label: '',
          model: null
        }]
      },
      interference1CheckBox1: {
        title: 'Interference with standing',
        model: null,
        field: 'checkbox',
        items: [{
          label: '',
          model: null
        }]
      },
      interference1Area: {
        title: 'Interference with standing',
        model: null,
        field: 'textarea'
      }
    },
    part80: {
      atrophyCheckBox: {
        title: 'Atrophy of disuse',
        model: null,
        field: 'checkbox',
        items: [{
          label: '',
          model: null
        }]
      },
      atrophyCheckBox1: {
        title: 'Atrophy of disuse',
        model: null,
        field: 'checkbox',
        items: [{
          label: '',
          model: null
        }]
      },
      atrophyArea: {
        title: 'Atrophy of disuse',
        model: null,
        field: 'textarea'
      }
    },
    part81: {
      instabilityCheckBox: {
        title: 'Instability of station',
        model: null,
        field: 'checkbox',
        items: [{
          label: '',
          model: null
        }]
      },
      instabilityCheckBox1: {
        title: 'Instability of station',
        model: null,
        field: 'checkbox',
        items: [{
          label: '',
          model: null
        }]
      },
      instabilityArea: {
        title: 'Instability of station',
        model: null,
        field: 'textarea'
      }
    },
    part82: {
      disturbanceCheckBox: {
        title: 'Disturbance of locomotion',
        model: null,
        field: 'checkbox',
        items: [{
          label: '',
          model: null
        }]
      },
      disturbanceCheckBox1: {
        title: 'Disturbance of locomotion',
        model: null,
        field: 'checkbox',
        items: [{
          label: '',
          model: null
        }]
      },
      disturbanceArea: {
        title: 'Disturbance of locomotion',
        model: null,
        field: 'textarea'
      }
    },
    part83: {
      othersText: {
        title: 'Disturbance of locomotion',
        label: 'Others, Describe',
        model: null,
        field: 'textfield',
        type: 'text'
      },
      othersCheckBox: {
        title: 'Disturbance of locomotion',
        model: null,
        field: 'checkbox',
        items: [{
          label: '',
          model: null
        }]
      },
      othersCheckBox1: {
        title: 'Disturbance of locomotion',
        model: null,
        field: 'checkbox',
        items: [{
          label: '',
          model: null
        }]
      },
      disturbanceArea: {
        title: 'Disturbance of locomotion',
        model: null,
        field: 'textarea'
      }
    },
    part84: {
      veteranQuestion: {
        title: '11A. Does the Veteran have any other pertinent physical findings, complications, conditions, signs or symptoms related to any conditions listed in the diagnosis section above?',
        model: null,
        field: 'button'
      },
      veteranQuestionShow: {
        title: 'If yes, describe (brief summary):',
        model: null,
        field: 'textarea'
      }
    },
    part85: {
      extremitiesQuestion: {
        title: 'Due to the Veteran’s shoulder or arm conditions, is there functional impairment of an extremity such that no effective function remains other than that which would be equally well served by an amputation with prosthesis? (Functions of the upper extremity include grasping, manipulation, etc., while functions for the lower extremity include balance and propulsion, etc.)',
        model: null,
        field: 'select'
      },
      extremitiesQuestion1: {
        title: 'If yes, describe (brief summary):',
        model: null,
        field: 'select'
      },
      extremitiesQuestion2: {
        title: 'For each checked extremity, identify the condition causing loss of function, describe loss of effective function and provide specific examples (brief summary):',
        model: null,
        field: 'textarea'
      }
    },
    part86: {
      noneImpackCheckBox: {
        title: 'None',
        model: null,
        field: 'checkbox',
        items: [{
          label: '',
          model: null
        }]
      },
      noneImpackCheckBox1: {
        title: 'None',
        model: null,
        field: 'checkbox',
        items: [{
          label: '',
          model: null
        }]
      },
      noneImpackArea: {
        title: 'None',
        model: null,
        field: 'textarea'
      }
    },
    part87: {
      movementCheckBox: {
        title: 'Repetitive movements',
        model: null,
        field: 'checkbox',
        items: [{
          label: '',
          model: null
        }]
      },
      movementCheckBox1: {
        title: 'Repetitive movements',
        model: null,
        field: 'checkbox',
        items: [{
          label: '',
          model: null
        }]
      },
      movementArea: {
        title: 'Repetitive movements',
        model: null,
        field: 'textarea'
      }
    },
    part88: {
      legsCheckBox: {
        title: 'Cross legs',
        model: null,
        field: 'checkbox',
        items: [{
          label: '',
          model: null
        }]
      },
      legsCheckBox1: {
        model: null,
        field: 'checkbox',
        items: [{
          label: '',
          model: null
        }]
      },
      legsArea: {
        title: 'Cross legs',
        model: null,
        field: 'textarea'
      }
    },
    part89: {
      weightCheckBox: {
        title: 'Prolonged weight-bearing',
        model: null,
        field: 'checkbox',
        items: [{
          label: '',
          model: null
        }]
      },
      weightCheckBox1: {
        title: 'Prolonged weight-bearing',
        model: null,
        field: 'checkbox',
        items: [{
          label: '',
          model: null
        }]
      },
      weightArea: {
        title: 'Prolonged weight-bearing',
        model: null,
        field: 'textarea'
      }
    },
    part90: {
      standingCheckBox: {
        title: 'Prolonged standing',
        model: null,
        field: 'checkbox',
        items: [{
          label: '',
          model: null
        }]
      },
      standingCheckBox1: {
        title: 'Prolonged standing',
        model: null,
        field: 'checkbox',
        items: [{
          label: '',
          model: null
        }]
      },
      standingArea: {
        title: 'Prolonged standing',
        model: null,
        field: 'textarea'
      }
    },
    part91: {
      sittingCheckBox: {
        title: 'Prolonged sitting',
        model: null,
        field: 'checkbox',
        items: [{
          label: '',
          model: null
        }]
      },
      sittingCheckBox1: {
        title: 'Prolonged sitting',
        model: null,
        field: 'checkbox',
        items: [{
          label: '',
          model: null
        }]
      },
      sittingArea: {
        title: 'Prolonged sitting',
        model: null,
        field: 'textarea'
      }
    },
    part92: {
      othersText: {
        label: 'Others, Describe',
        model: null,
        field: 'textfield',
        type: 'text'
      },
      othersCheckBox: {
        model: null,
        field: 'checkbox',
        items: [{
          label: '',
          model: null
        }]
      },
      othersCheckBox1: {
        model: null,
        field: 'checkbox',
        items: [{
          label: '',
          model: null
        }]
      },
      othersArea: {
        model: null,
        field: 'textarea'
      }
    },
    part93: {
      remarks: {
        model: null,
        field: 'textarea'
      }
    },
    part94: {
      serviceDiagnosis: {
        title: 'SERVICE-CONNECTED DIAGNOSIS:',
        model: null,
        field: 'textarea'
      }
    },
    part95: {
      claimText: {
        label: 'Others, Describe',
        model: null,
        field: 'textfield',
        type: 'text'
      },
      claimSelect: {
        model: null,
        field: 'select'
      },
      claimText1: {
        model: null,
        field: 'textfield',
        type: 'text'
      }
    },
    part96: {
      rationale: {
        model: null,
        field: 'textarea'
      }
    }
  })
}
